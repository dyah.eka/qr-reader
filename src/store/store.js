import { createStore } from "vuex";
import * as mutations from "./mutations";

const store = createStore({
    state: {
		result: {
            fromVideo: {
                result: null,
                warningMessage: null
            },
            fromImage: {
                result: null,
                warningMessage: null
            },
        },
    },
    mutations: {
        [mutations.SET_RESULT_VID] (state, payload) {
            state.result.fromVideo.result = payload;
        },
        [mutations.SET_WARNING_MESSAGE_VID] (state, payload) {
            state.result.fromVideo.warningMessage = payload;
        },
        [mutations.SET_RESULT_IMG] (state, payload) {
            state.result.fromImage.result = payload;
        },
        [mutations.SET_WARNING_MESSAGE_IMG] (state, payload) {
            state.result.fromImage.warningMessage = payload;
        },
    },
})

export default store;