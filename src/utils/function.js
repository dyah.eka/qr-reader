import { BrowserCodeReader, BrowserQRCodeReader } from '@zxing/browser';


// get Data from video element
export const getData = async (previewElem, callback) => {
    const codeReader = new BrowserQRCodeReader();
    const videoInputDevices = await BrowserCodeReader.listVideoInputDevices();
    const selectedDeviceId = videoInputDevices[0].deviceId;

    // console.log(`Started decode from camera with id ${selectedDeviceId}`);

    let gotResult = false;

   const controls = await codeReader.decodeFromVideoDevice(selectedDeviceId, previewElem, (result, error, controls) => {
        if (result !== undefined) {
            controls.stop();
            gotResult = true;
            callback(null, result.text)
        }
    });

    setTimeout(() => {
        controls.stop()
        if (!gotResult) {
            const warningMessage = "Are you there?";
            callback(warningMessage, null)
        }
    }, 20000);
}

// get Data from image uploaded
export const getDataFromUpload = async (source, callback) => {
    const codeReader = new BrowserQRCodeReader();
    const base64 = source;
    
    try {
        const resultImage = await codeReader.decodeFromImageUrl(base64);
        callback(null, resultImage.text)
    } catch (error) {
        callback("Not found", null)
    }
}