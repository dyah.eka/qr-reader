import { createRouter, createWebHistory } from 'vue-router'
import QRCode from '../views/QRCode.vue'

const QRCodeImage = () => import("../views/QRCodeImage.vue")

const routes = [
  {
    path: '/',
    name: 'QRCode',
    component: QRCode
  },
  {
    path: '/upload-image',
    name: 'QRCodeImage',
    component: QRCodeImage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
