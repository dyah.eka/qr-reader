import { shallowMount } from '@vue/test-utils'
import Result from '@/components/Result.vue'

describe('Result.vue', () => {
  it('has props', () => {
    expect(Result.props).toBeDefined();
  })

  it('has result as props', () => {
    expect(Result.props).toContain('result');
  })

  it('renders props.result when passed', () => {
    const result = 'result of qr code'
    const wrapper = shallowMount(Result, {
      props: { result }
    })
    expect(wrapper.text()).toMatch(result)
  })
})
